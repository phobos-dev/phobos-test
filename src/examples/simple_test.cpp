#include "../include/phobos_test.hpp"

using namespace phobos_test;

TestCase case1([](){
    Assert(2 * 2 == 5);
    Assert("" == "");
    Assert(2 == 3);
});


int main(int _argc, const char * const * _argv)
{
    TestSuite suite1("simple_test");
    suite1.AddTest(case1);

    TestExecutor executor(_argc, _argv);
    executor.AddSuite(suite1);

    return executor.Execute();
}