#include "../include/phobos_test2.hpp"

#include <iostream>

using namespace phobos_test2;


class ConsoleLogger : public Logger
{
public:
    void log(const std::string &str) override
    {
        std::cout << str << std::endl;
    }
};


int main(int argc, const char * const * argv)
{
    TestSuite suite{"My Suite"};

    using MyCase = struct : TestSuite::Case {
        void operator()()
        {
            assert.equal("WTF1", 1, 2);
            assert.not_equal("WTF2", 1, 1);
        }
    };

    suite.add_case<MyCase>("Simple Case");

    ConsoleLogger logger;
    execute({&suite}, logger);

    return 0;
}