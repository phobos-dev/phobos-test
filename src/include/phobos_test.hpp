#pragma once

#ifndef __PHOBOS_TEST__
#define __PHOBOS_TEST__

#include <vector>
#include <string>
#include <memory>

namespace phobos_test
{

// Use this inside the test body to check assertions
void Assert(bool _condition);

enum class TEST_RESULT : int
{
    SUCCESS,
    FAIL
};

//

class TestCase;

class TestContext
{
public:
    static TestContext * Instance();

    void OnTestEnter(TestCase * _testCase);
    void OnTestLeave(TestCase * _testCase);
    void OnCheckAssertion(bool _assertion, const std::string & _comment = "");

private:
    TestContext();

private:
    TestCase * runningTestCase;
    int        assertionNumber;
};

//

class TestCase
{
public:
    TestCase(std::function<void()> _testFunction);
    void Begin();
    bool Execute(); // TODO: return some 'results' structure, not plain code
    void End();

    void SetReason(const std::string & _message);
    void Fail(int _ordinal, const std::string & _message);

private:
    std::function<void()> testFunction;
    bool                  failed;
    std::vector<std::string> reasons;
};

//

class TestSuite
{
public:
    TestSuite(const std::string & _name);
    void AddTest(const TestCase & _testCase);
    TEST_RESULT Execute();

private:
    std::string           name;
    std::vector<TestCase> tests;
};

//

class TestExecutor
{
public:
    TestExecutor(int & _argc, const char * const * _argv);

    void AddSuite(const TestSuite & _suite);

    int Execute();

private:
    void ParseOptions(int & _argc, const char * const * _argv);

private:
    std::vector<TestSuite> suites;
};

//

}

#endif