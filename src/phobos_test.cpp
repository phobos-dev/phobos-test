#include "include/phobos_test.hpp"
#include <cassert> // TODO: make internal error messages instead of C assertions

using namespace std;

namespace phobos_test
{

void Assert(bool _condition)
{
    TestContext::Instance()->OnCheckAssertion(_condition);
}

//

TestContext * TestContext::Instance()
{
    static TestContext context;
    return &context;
}

void TestContext::OnTestEnter(TestCase * _testCase)
{
    assertionNumber = 0;
    runningTestCase = _testCase;
}

void TestContext::OnTestLeave(TestCase * _testCase)
{
    assert(_testCase == runningTestCase);
    runningTestCase = nullptr;
}

void TestContext::OnCheckAssertion(bool _assertion, const string & _comment)
{
    ++assertionNumber;
    if (runningTestCase && !_assertion)
        runningTestCase->Fail(assertionNumber, _comment);
}

TestContext::TestContext()
{
}

//

TestCase::TestCase(std::function<void()> _testFunction):
    testFunction(_testFunction),
    failed(false)
{
}

void TestCase::Begin()
{
    TestContext::Instance()->OnTestEnter(this);
}

bool TestCase::Execute()
{
    if (testFunction)
        testFunction();
    return failed;
}

void TestCase::End()
{
    TestContext::Instance()->OnTestLeave(this);
}

void TestCase::SetReason(const std::string & _message)
{
    reasons.push_back(_message);
}

void TestCase::Fail(int _ordinal, const std::string & _message)
{
    printf("Test assertion #%d failed! %s\n", _ordinal, _message.c_str());
    failed = true;
    SetReason(_message);
}

//

TestSuite::TestSuite(const std::string & _name) :
    name(_name)
{
}

void TestSuite::AddTest(const TestCase & _testCase)
{
    tests.push_back(_testCase);
}

TEST_RESULT TestSuite::Execute()
{
    TEST_RESULT result = TEST_RESULT::SUCCESS;
    for (decltype(auto) test : tests)
    {
        test.Begin();
        if (!test.Execute())
            result = TEST_RESULT::FAIL;
        test.End();
    }
    return result;
}

// TestExecutor

TestExecutor::TestExecutor(int & _argc, const char * const * _argv)
{
    ParseOptions(_argc, _argv);
}

void TestExecutor::AddSuite(const TestSuite & _suite)
{
    suites.push_back(_suite);
}

int TestExecutor::Execute()
{
    TEST_RESULT result = TEST_RESULT::SUCCESS;

    for (decltype(auto) suite : suites)
        if (suite.Execute() == TEST_RESULT::FAIL)
            result = TEST_RESULT::FAIL;

    return static_cast<int>(result);
}

void TestExecutor::ParseOptions(int & _argc, const char * const * _argv)
{
    // TODO: command-line options: json output, for example
}

//

}